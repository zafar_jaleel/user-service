package com.excellarate.service;

import com.excellarate.dto.UserDTO;

public interface UserService {

	public UserDTO createUser(UserDTO userDTO);
	
	public UserDTO getUser(Long userId);
}
