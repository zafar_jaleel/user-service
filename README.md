# User-Service

# Steps to Complie & build
From root directory execute ./gradlew clean build -x test

# Setup Database

docker run -d -p 3308:3306 -e MYSQL_USER=useradmin -e MYSQL_PASSWORD=useradmin -e MYSQL_ROOT_PASSWORD=useradmin  -e MYSQL_DATABASE=user-service --name user-db mysql:5.7

# Run Application

After build is successful go to

build/libs folder and execute

"java -jar *.jar

# Access Application

http://localhost:9003/swagger-ui.html
